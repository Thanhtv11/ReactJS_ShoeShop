import React, { Component } from "react";
import { shoeArr } from "./data_shoeShop";
import ItemShoe from "./ItemShoe";
import TableGioHang from "./TableGioHang";

export default class Ex_ShoeShop extends Component {
  state = {
    shoeArr: shoeArr,
    gioHang: [],
  };

  showShoes = () => {
    return this.state.shoeArr.map((item) => {
      return (
        <ItemShoe
          handleAddNew={this.handleAddNew}
          shoeData={item}
          key={item.id}
        />
      );
    });
  };

  handleAddNew = (sp) => {
    let index = this.state.gioHang.findIndex((item) => {
      return item.id == sp.id;
    });
    let GioHangCopy = [...this.state.gioHang];
    if (index == -1) {
      let newSp = { ...sp, soLuong: 1 };
      GioHangCopy.push(newSp);
    } else {
      GioHangCopy[index].soLuong++;
    }
    this.setState({
      gioHang: GioHangCopy,
    });
  };

  handleRemoveShoe = (idShoe) => {
    let index = this.state.gioHang.findIndex((item) => {
      return item.id == idShoe;
    });
    if (index !== -1) {
      let GioHangCopy = [...this.state.gioHang];
      GioHangCopy.splice(index, 1);
      this.setState({ gioHang: GioHangCopy });
    }
  };

  handleChangeQuantity = (idShoe, step) => {
    let index = this.state.gioHang.findIndex((item) => {
      return item.id == idShoe;
    });
    let GioHangCopy = [...this.state.gioHang];
    GioHangCopy[index].soLuong = GioHangCopy[index].soLuong + step;
    if (GioHangCopy[index].soLuong == 0) {
      GioHangCopy.splice(index, 1);
    }
    this.setState({ gioHang: GioHangCopy },);
  };
  render() {
    return (
      <div className="container py-5">
        {this.state.gioHang.length > 0 && (
          <TableGioHang
            handleRemoveShoe={this.handleRemoveShoe}
            gioHang={this.state.gioHang}
            handleChangeQuantity={this.handleChangeQuantity}
          />
        )}
        <div className="row">{this.showShoes()}</div>
      </div>
    );
  }
}
